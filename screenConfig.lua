local ecs = require("ECSAPI")
local components = require("component")
local serialization = require("serialization")
local fs = require("filesystem")
local event = require("event")
local io = require("io")
local term = require("term")
local utils = require("utils")
local multiScreen = require("multiscreen")
local gpu = components.gpu

local pathToConfigFolder = "cfg/"
local pathToConfigFile = pathToConfigFolder .. "./Monitors.cfg"

local screenOnFloor = {}
local screenInsideLift = {}

local screenResolution = {
  width = 30,
  height = 15
}

local mainScreenWidth, mainScreenHeight = gpu.getResolution()
local mainScreenAddress = gpu.getScreen()

----------

local function getAllConnectedScreens()
	local massiv = {}
	for address in pairs(components.list("screen")) do
		table.insert(massiv, address)
	end
	return massiv
end

local function saveConfig()
	local file = io.open(pathToConfigFile, "w")
	file:write(serialization.serialize({screenOnFloor, screenInsideLift}))
	file:close()
end

local function loadConfig()
	if fs.exists(pathToConfigFile) then
		local file = io.open(pathToConfigFile, "r")
		local tmp = serialization.unserialize(file:read("*a"))
    screenOnFloor = tmp[1]
    screenInsideLift = tmp[2]
		file:close()
		print("Конфиг успешно загружен")
	else
		print("Конфиг не найден!")
    os.exit()
	end
end

local function drawCalibrateScreen(width, height, curX, curY)
    utils.clearScreen()
    local w, h = 4, 2
    local xC, yC = 1, 1
		local x, y = 3, 2
		local xPos, yPos = x, y
		for j = 1, height do
			for i = 1, width do
				if j == curX and i == curY then
					ecs.square(xPos, yPos, w, h, ecs.colors.green)
				else
					ecs.square(xPos, yPos, w, h, 0xDDDDDD)
				end
				xPos = xPos + w + 2
			end
			yPos = yPos + h + 1
			xPos = x
		end
		gpu.setBackground(0x000000)
		gpu.setForeground(0xffffff)
	end

local function calibrate()
  --Окно настроек
  local widnow = ecs.universalWindow("auto", "auto", 40, 0xeeeeee, true, {"EmptyLine"}, {"CenterText", 0x880000, "Здорово, ебана!"}, {"EmptyLine"}, {"WrappedText", 0x262626, "Необходимо указать количество этажей"}, {"EmptyLine"}, {"Input", 0x262626, 0x880000, "Этажи"}, {"EmptyLine"}, {"Button", {ecs.colors.orange, 0xffffff, "Подтвердить"}, {0x777777, 0xffffff, "Отмена"}})
	local floorCount = tonumber(widnow[1])
	if widnow[2] == "Отмена" then
		print("Настройка отменена!")
		os.exit()
	end
  
  local screens = getAllConnectedScreens()
  --На экране компа X сверху вниз, а этажи нумеруются в обратную сторону
  local curX = 1;
  -- 1 - экран снаружи; 2 - экран внутри
  local curY = 1;
  --Такого быть не должно, чисто на всякий случай
  local position = "ошибка!"
  --Уже "использованные" экраны, при нажатии на них ничего не происходит
  local calibratedScreen = {}
  while true do
    if curY == 1 then
      position = "на этаже"
    elseif curY == 2 then
      position = "внутри лифта"
    end
    --Рисуем на всех доступных экранах интерфейс калибровки
    for i = 1, #screens do
      if not utils.HasValueInTable(calibratedScreen, screens[i]) then
        gpu.bind(screens[i]);
        if screens[i] == mainScreenAddress then
          gpu.setResolution(mainScreenWidth, mainScreenHeight)
          utils.clearScreen()
          ecs.square(1, 1, mainScreenWidth, mainScreenHeight, 0x2200AA)
          ecs.centerText("x", math.floor(mainScreenHeight / 2) - 1, "Идёт настройка экранов..")
          ecs.centerText("x", math.floor(mainScreenHeight / 2), "Текущий экран:")
          ecs.centerText("x", math.floor(mainScreenHeight / 2) + 1, curX .. " этаж, " .. position)
        else
          gpu.setResolution(screenResolution.width, screenResolution.height)
          utils.clearScreen() 
          ecs.square(1, 1, screenResolution.width, screenResolution.height, 0x2200AA)
          ecs.centerText("x", math.floor(screenResolution.height / 2) - 1, "Коснитесь экрана:")
          ecs.centerText("x", math.floor(screenResolution.height / 2) + 1, curX .. " этаж, " .. position)
        end
      end
    end
    --отлавливание нажатий
    local tmp = multiScreen.find()
    --Нажатия с главного экрана не ловим, на всякий случай
    if not utils.HasValueInTable(calibratedScreen, tmp) and tmp ~= mainScreenAddress then
      gpu.bind(tmp)
      gpu.setResolution(screenResolution.width, screenResolution.height)
      ecs.square(1, 1, screenResolution.width, screenResolution.height, 0x00FF00)
      gpu.setForeground(0xffffff)      
      ecs.centerText("xy", 0, curX .. " этаж, " .. position)
      table.insert(calibratedScreen, tmp)
      if curY == 1 then
        table.insert(screenOnFloor, tmp)
        curY = curY + 1
      else
        table.insert(screenInsideLift, tmp)
        curY = 1
        curX = curX + 1
      end
      if curX == floorCount + 1 then
        break
      end
    end
  end
  os.sleep(3)
  for i = 1, #screens do
    gpu.bind(screens[i])
    gpu.setResolution(screenResolution.width, screenResolution.height)
    utils.clearScreen()
  end
  gpu.bind(mainScreenAddress)
  gpu.setResolution(mainScreenWidth, mainScreenHeight)
  utils.clearScreen()
  term.write("Готово!")
  saveConfig()
end

local function check() 
  for i = 1, #screenOnFloor do
    gpu.bind(screenOnFloor[i]);
    gpu.setResolution(screenResolution.width, screenResolution.height)
    utils.clearScreen() 
    ecs.square(1, 1, screenResolution.width, screenResolution.height, 0xFFBB00)
    ecs.centerText("x", math.floor(screenResolution.height / 2), i.. " этаж, на этаже")
  end
  for i = 1, #screenInsideLift do
    gpu.bind(screenInsideLift[i]);
    gpu.setResolution(screenResolution.width, screenResolution.height)
    utils.clearScreen() 
    ecs.square(1, 1, screenResolution.width, screenResolution.height, 0xFFBB00)
    ecs.centerText("x", math.floor(screenResolution.height / 2), i.. " этаж, внутри лифта")
  end
  gpu.bind(mainScreenAddress);
  term.write("Готово!")
  saveConfig()
end

----------

local args = {...}

if args[1] == "calibrate" then
  fs.makeDirectory(pathToConfigFolder)
	fs.remove(pathToConfigFile)
  calibrate()
elseif args[1] == "clear" then
	loadConfig()
	multiScreen.clearAll(mainScreenAddress, screenOnFloor, screenResolution, tonumber(args[2] or 0x000000))
  multiScreen.clearAll(mainScreenAddress, screenInsideLift, screenResolution, tonumber(args[2] or 0x000000))
elseif args[1] == "check" then
	loadConfig()
	check()
else
	print("Использование программы:")
	print("  screenConfig calibrate - перекалибровать мониторы")
	print("  screenConfig clear <цвет> - залить мониторы указанным цветом (черный по умолчанию)")
  print("  screenConfig check - Вывод положения на мониторы")
end