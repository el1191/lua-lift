local components = require("component")
local gpu = components.gpu
local term = require("term")

local utils = {}

function utils.clearScreen()
  gpu.setBackground(0x000000)
	gpu.setForeground(0xFFFFFF)
  term.setCursor(1, 1)
  term.clear()
	gpu.set(1, 1, "")
end

function utils.HasValueInTable(table, value)
    for index, val in pairs(table) do
        if val == value then
            return true
        end
    end
    return false
end

return utils