local ecs = require("ECSAPI")
local event = require("event")
local component = require("component")
local gpu = component.gpu

local multiScreen = {}

--Заливка всех экранов из массива и возврат видеокарты на основной
function multiScreen.clearAll(mainScreenAddress, screenArrray, screenResolution, color)
  for i = 1, #screenArrray do
    gpu.bind(screenArrray[i])
    gpu.setResolution(screenResolution.width, screenResolution.height)
    gpu.setBackground(color or 0x000000)
    gpu.fill(1, 1, screenResolution.width, screenResolution.height, " ")
    gpu.bind(mainScreenAddress)
  end
end

--Вернёт адресс любого экрана, кроме основного
function multiScreen.find(mainScreenAddress)
  local touched = nil
  while touched == nil do
    local e = {event.pull()}
    if e[1] == "touch" then
      touched = e[2]
    end
  end  
  return touched
end

return multiScreen